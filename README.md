# bloomberg-deals

How to start the bloomberg-deals application
---

1. Run `mvn clean install` to build your application
1. Run the (/src/test/resources)deals-schema file in your Mysql server.
1. set your database configurations in config.yml
1. Start application with `java -jar target/bloomberg-1.0.0.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

<h6>Note:</h6><i>kindly use the <b>import_deals.csv</b> sample file under resources folder, which contains 100k deals (80k as valid deals) and (20k as invalid deals)