package com.propgresssoft.bloomberg.beans.deals;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * this Entity presents the imported file information,
 * implements {@link DealMarker}
 */

@Entity
@Table(name = "imported_files")
public class ImportedCsvFile implements DealMarker{

    @Id
    @Column(name = "file_name")
    private String fileName;
    private int success;
    private int failed;

    public ImportedCsvFile() {
    }

    public ImportedCsvFile(String fileName, int success, int failed) {
        this.fileName = fileName;
        this.success = success;
        this.failed = failed;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
