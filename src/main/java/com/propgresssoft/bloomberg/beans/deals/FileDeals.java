package com.propgresssoft.bloomberg.beans.deals;

import java.util.List;
import java.util.Map;

/**
 * this is the main class which contains all csv file imports
 */
public class FileDeals {

    private List<SuccessDeal> validDeals;
    private List<FailedDeal> inValidDeals;
    private Map<String,Integer> currencies;
    private ImportedCsvFile importedFile;

    public List<SuccessDeal> getValidDeals() {
        return validDeals;
    }

    public void setValidDeals(List<SuccessDeal> validDeals) {
        this.validDeals = validDeals;
    }

    public List<FailedDeal> getInValidDeals() {
        return inValidDeals;
    }

    public void setInValidDeals(List<FailedDeal> inValidDeals) {
        this.inValidDeals = inValidDeals;
    }

    public Map<String, Integer> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(Map<String, Integer> currencies) {
        this.currencies = currencies;
    }

    public ImportedCsvFile getImportedFile() {
        return importedFile;
    }

    public void setImportedFile(ImportedCsvFile importedFile) {
        this.importedFile = importedFile;
    }
}
