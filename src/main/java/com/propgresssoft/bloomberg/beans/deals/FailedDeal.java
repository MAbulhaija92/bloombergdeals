package com.propgresssoft.bloomberg.beans.deals;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * this Entity presents in valid deals from csv file,
 * implements {@link DealMarker}
 */
@Entity
@Table(name = "failed_imported_deals")
public class FailedDeal implements DealMarker{
    @Id
    @Column(name = "deal_id")
    private String dealId;
    @Column(name = "deal_raw")
    private String dealRaw;
    @Column(name = "from_file")
    private String fromFile;
    private String reason;

    public FailedDeal() {
    }

    public FailedDeal(String dealId, String dealRaw, String fromFile, String reason) {
        this.dealId = dealId;
        this.dealRaw = dealRaw;
        this.fromFile = fromFile;
        this.reason = reason;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getDealRaw() {
        return dealRaw;
    }

    public void setDealRaw(String dealRaw) {
        this.dealRaw = dealRaw;
    }

    public String getFromFile() {
        return fromFile;
    }

    public void setFromFile(String fromFile) {
        this.fromFile = fromFile;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
