package com.propgresssoft.bloomberg.beans.deals;

import javax.persistence.*;
import java.util.Objects;

/**
 * this Entity presents each currency with success deals count,
 * implements {@link DealMarker}
 */
@Entity
@Table(name = "currency_statistics")
public class CurrencySummary implements DealMarker{

    @Id
    @Column(name = "currency_iso_code")
    private String currencyCode;
    @Column(name = "success_deals_count")
    private Integer successDeals;


    public CurrencySummary() {
    }

    public CurrencySummary(String currencyCode, Integer successDeals) {
        this.currencyCode = currencyCode;
        this.successDeals = successDeals;

    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Integer getSuccessDeals() {
        return successDeals;
    }

    public void setSuccessDeals(Integer successDeals) {
        this.successDeals = successDeals;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencySummary that = (CurrencySummary) o;
        return Objects.equals(currencyCode, that.currencyCode);
    }

    @Override
    public int hashCode() {

        return Objects.hash(currencyCode);
    }
}
