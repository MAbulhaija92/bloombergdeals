package com.propgresssoft.bloomberg.beans.deals;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * this Entity presents the valid deals from csv file,
 * implements {@link DealMarker}
 */

@Entity
@Table(name = "success_imported_deals")
public class SuccessDeal implements DealMarker{

    @Id
    @Column(name = "deal_id")
    private String dealId;
    @Column(name = "from_currency")
    private String formCurrencyISO;
    @Column(name = "to_currency")
    private String toCurrencyISO;
    @Column(name = "deal_date")
    private Timestamp dealTimeStamp;
    @Column(name = "amount")
    private double dealAmount;
    @Column(name = "from_file")
    private String fromFile;

    public SuccessDeal() {
    }

    public SuccessDeal(String dealId, String formCurrencyISO, String toCurrencyISO, Timestamp dealTimeStamp, double dealAmount, String fromFile) {
        this.dealId = dealId;
        this.formCurrencyISO = formCurrencyISO;
        this.toCurrencyISO = toCurrencyISO;
        this.dealTimeStamp = dealTimeStamp;
        this.dealAmount = dealAmount;
        this.fromFile = fromFile;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getFormCurrencyISO() {
        return formCurrencyISO;
    }

    public void setFormCurrencyISO(String formCurrencyISO) {
        this.formCurrencyISO = formCurrencyISO;
    }

    public String getToCurrencyISO() {
        return toCurrencyISO;
    }

    public void setToCurrencyISO(String toCurrencyISO) {
        this.toCurrencyISO = toCurrencyISO;
    }

    public Timestamp getDealTimeStamp() {
        return dealTimeStamp;
    }

    public void setDealTimeStamp(Timestamp dealTimeStamp) {
        this.dealTimeStamp = dealTimeStamp;
    }

    public double getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(double dealAmount) {
        this.dealAmount = dealAmount;
    }

    public String getFromFile() {
        return fromFile;
    }

    public void setFromFile(String fromFile) {
        this.fromFile = fromFile;
    }
}
