package com.propgresssoft.bloomberg.beans;

import com.propgresssoft.bloomberg.beans.deals.FileDeals;

/**
 * class to represent the response returns when the Import finished successfully
 */
public class ImportResult {

    private FileDeals fileDeals;
    private long millis;

    public ImportResult() {
    }

    public ImportResult(FileDeals fileDeals, long seconds) {
        this.fileDeals = fileDeals;
        this.millis = seconds;
    }

    public FileDeals getFileDeals() {
        return fileDeals;
    }

    public void setFileDeals(FileDeals fileDeals) {
        this.fileDeals = fileDeals;
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }
}
