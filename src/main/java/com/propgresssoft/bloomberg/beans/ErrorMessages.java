package com.propgresssoft.bloomberg.beans;

/**
 * this class contains the main error messages used through the system
 */
public class ErrorMessages {

    public static final String NOT_SUPPORTED_FILE_TYPE = "file type not supported,Please select CSV files";
    public static final String EMPTY_FILE_ERROR = "file is Empty";
    public static final String COULD_NOT_READ_CSV_ROW = "couldn't read csv row form File";
    public static final String FILE_IMPORTED_BEFORE = "File imported before please select new file";
    public static final String FILE_NOT_FOUND = "File you are requesting does not exist";

    // validations errors
    public static final String DEAL_ID_ERROR = "Invalid deal Id";
    public static final String FROM_CURRENCY_CODE_ERROR = "Invalid From Currency code";
    public static final String TO_CURRENCY_CODE_ERROR = "Invalid To Currency code";
    public static final String DEAL_TIMESTAMP_ERROR = "Invalid deal time stamp";
    public static final String DEAL_AMOUNT_ERROR = "Invalid deal amount";
    public static final String INVALID_COLUMNS_SIZE = "Invalid columns size";
}
