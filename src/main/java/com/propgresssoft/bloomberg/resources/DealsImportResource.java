package com.propgresssoft.bloomberg.resources;


import com.propgresssoft.bloomberg.beans.ErrorMessages;
import com.propgresssoft.bloomberg.beans.FailResponse;
import com.propgresssoft.bloomberg.beans.ImportResult;
import com.propgresssoft.bloomberg.beans.deals.FailedDeal;
import com.propgresssoft.bloomberg.beans.deals.FileDeals;
import com.propgresssoft.bloomberg.beans.deals.SuccessDeal;
import com.propgresssoft.bloomberg.core.business.DealsImportBusiness;
import com.propgresssoft.bloomberg.core.exceptions.FileImportException;
import com.propgresssoft.bloomberg.core.exceptions.FileValidationException;
import com.propgresssoft.bloomberg.db.DealsDAO;
import io.dropwizard.hibernate.UnitOfWork;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Path("/")
public class DealsImportResource {

    private static final Logger logger = LoggerFactory.getLogger(DealsImportResource.class);
    private final DealsDAO dao;

    public DealsImportResource(DealsDAO dao) {

        this.dao = dao;
    }

    @POST
    @Path("import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response UploadCsvFile(@FormDataParam("file") InputStream csvFile,
                                  @FormDataParam("file") FormDataContentDisposition formDataContentDisposition) {

        try {
            DealsImportBusiness.validateCSVFile(formDataContentDisposition);
            if (dao.getFileIfImported(DealsImportBusiness.getFileNameWithoutType(formDataContentDisposition.getFileName())) != null) {

                throw new FileValidationException(ErrorMessages.FILE_IMPORTED_BEFORE);
            }

            FileDeals fileDeals = DealsImportBusiness.fetchDealsFromCSV(csvFile, formDataContentDisposition.getFileName());

            ImportResult result = ImportFileToDB(fileDeals);
            fileDeals.setValidDeals((List<SuccessDeal>) DealsImportBusiness.getFirstPageFromList(fileDeals.getValidDeals()));
            fileDeals.setInValidDeals((List<FailedDeal>) DealsImportBusiness.getFirstPageFromList(fileDeals.getInValidDeals()));

            return Response.ok(result).build();

        } catch (FileImportException e) {
            logger.error(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new FailResponse(e.getMessage(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())).build();
        } catch (FileValidationException e) {

            logger.error(e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity(new FailResponse(e.getMessage(), Response.Status.BAD_REQUEST.getStatusCode())).build();
        }
    }

    @GET
    @Path("info")
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFileInfo(@NotEmpty @QueryParam("file_name") String fileName,
                                @NotNull @QueryParam("page") Integer page) {
        try {
            FileDeals importedFileResults = dao.getImportedFileResults(fileName, page);
            return Response.ok(importedFileResults).build();
        } catch (FileImportException e) {
            return Response.status(Response.Status.NOT_FOUND).entity(new FailResponse(ErrorMessages.FILE_NOT_FOUND, Response.Status.NOT_FOUND.getStatusCode())).build();
        }
    }

    private ImportResult ImportFileToDB(FileDeals fileDeals) {

        ImportResult result = new ImportResult();

        long start = System.currentTimeMillis();

        dao.importSuccessDeals(fileDeals.getValidDeals());
        dao.importFailedDeals(fileDeals.getInValidDeals());
        dao.importCurrenciesSummary(fileDeals.getCurrencies());
        dao.insertFile(fileDeals.getImportedFile());


        result.setMillis(TimeUnit.MILLISECONDS.toMillis(System.currentTimeMillis() - start));
        result.setFileDeals(fileDeals);
        return result;
    }
}
