package com.propgresssoft.bloomberg.core.business;


import com.propgresssoft.bloomberg.beans.ErrorMessages;
import com.propgresssoft.bloomberg.beans.deals.*;
import com.propgresssoft.bloomberg.core.exceptions.FileImportException;
import com.propgresssoft.bloomberg.core.exceptions.FileValidationException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.*;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * this class presents the main methods to handle the imported csv file
 */
public class DealsImportBusiness {

    public static final String CSV_TYPE = ".csv";
    public static final int DEFAULT_PAGE_SIZE = 30;

    private static final int COLUMNS_SIZE = 5;
    private static final int DEAL_ID_INDEX = 0;
    private static final int FROM_CURRENCY_INDEX = 1;
    private static final int TO_CURRENCY_INDEX = 2;
    private static final int DEAL_TIMESTAMP_INDEX = 3;
    private static final int DEAL_AMOUNT_INDEX = 4;
    private static final Logger LOGGER = LoggerFactory.getLogger(DealsImportBusiness.class);

    /**
     * method to get deals from the imported file
     *
     * @param inputStream, file input stream
     * @param fileName,    the file name
     * @return {@link FileDeals} object,contains all imported data and file info
     * @throws FileImportException , in case some thing went wrong
     */
    public static FileDeals fetchDealsFromCSV(InputStream inputStream, String fileName) throws FileImportException {

        fileName = getFileNameWithoutType(fileName);
        FileDeals fileDeals = new FileDeals();

        List<SuccessDeal> successDeals = new ArrayList<>();
        List<FailedDeal> failedDeals = new ArrayList<>();
        Map<String, Integer> currencies = new HashMap<>();

        char delimiter = ',';
        char quoteChar = '"';
        CsvPreference csvPreference = new CsvPreference.Builder(quoteChar, delimiter, "\r\n").build();

        try (CsvListReader reader = new CsvListReader(new InputStreamReader(inputStream), csvPreference)) {
            List<String> columns;
            while ((columns = reader.read()) != null) {
                if (columns.isEmpty() || columns.size() < 1) {
                    throw new FileImportException(ErrorMessages.COULD_NOT_READ_CSV_ROW);
                }
                FailedDeal failedDeal = validateAndGetFailedDeal(columns, fileName);
                if (failedDeal != null) {
                    failedDeals.add(failedDeal);
                } else {
                    SuccessDeal successDeal = new SuccessDeal(columns.get(DEAL_ID_INDEX), columns.get(FROM_CURRENCY_INDEX), columns.get(TO_CURRENCY_INDEX), Timestamp.valueOf(columns.get(DEAL_TIMESTAMP_INDEX)), Double.valueOf(columns.get(DEAL_AMOUNT_INDEX)), fileName);
                    if (currencies.containsKey(columns.get(FROM_CURRENCY_INDEX))) {
                        currencies.put(columns.get(FROM_CURRENCY_INDEX), currencies.get(columns.get(FROM_CURRENCY_INDEX)) + 1);
                    } else {
                        currencies.put(columns.get(FROM_CURRENCY_INDEX), 1);
                    }
                    successDeals.add(successDeal);
                }

            }

        } catch (IOException e) {
            throw new FileImportException(e.getMessage());
        }
        fileDeals.setCurrencies(currencies);
        fileDeals.setInValidDeals(failedDeals);
        fileDeals.setValidDeals(successDeals);
        fileDeals.setImportedFile(new ImportedCsvFile(fileName, successDeals.size(), failedDeals.size()));
        return fileDeals;
    }

    /**
     * validate if the file uploaded is valid as type and size
     * @param formDataContentDisposition, contains file information
     * @throws FileValidationException , in case file is not valid
     */
    public static void validateCSVFile(FormDataContentDisposition formDataContentDisposition) throws FileValidationException {
        if (formDataContentDisposition == null || !formDataContentDisposition.getFileName().toLowerCase().endsWith(DealsImportBusiness.CSV_TYPE)) {
            throw new FileValidationException(ErrorMessages.NOT_SUPPORTED_FILE_TYPE);
        }
        if (formDataContentDisposition.getSize() == 0) {
            throw new FileValidationException(ErrorMessages.EMPTY_FILE_ERROR);
        }
    }

    /**
     * this method trims the file type (.csv) from file name
     *
     * @param fileName, full file name with type i.e filename.csv
     * @return {@link String} the file name without type i.e filename
     */
    public static String getFileNameWithoutType(String fileName) {
        if (!fileName.endsWith(DealsImportBusiness.CSV_TYPE)) {
            return fileName;
        }
        return fileName.substring(0, fileName.lastIndexOf(DealsImportBusiness.CSV_TYPE));
    }


    public static List<? extends DealMarker> getFirstPageFromList(List<? extends DealMarker> deals) {
        if (deals.size() > DEFAULT_PAGE_SIZE) {
            return deals.subList(0, DEFAULT_PAGE_SIZE);
        } else {
            return deals;
        }
    }

    /**
     * validate the deal timestamp
     *
     * @param timeStampString
     * @return boolean
     */
    private static boolean isValidTimeStamp(String timeStampString) {
        try {
            Timestamp.valueOf(timeStampString);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }


    /**
     * this method validate the csv row columns,
     *
     * @param columns,  row columns from csv file
     * @param fileName, the file name containing the csv rows
     * @return {@link FailedDeal}, in case one or more rows was failed, else return null
     */
    private static FailedDeal validateAndGetFailedDeal(List<String> columns, String fileName) {

        String rawDeal = getRawDealString(columns);

        if (isBlank(columns.get(DEAL_ID_INDEX))) {
            LOGGER.error(ErrorMessages.DEAL_ID_ERROR);
            return new FailedDeal("BLANK" + RandomStringUtils.random(5, true, true), rawDeal, fileName, ErrorMessages.DEAL_ID_ERROR);
        }
        String dealId = columns.get(DEAL_ID_INDEX);
        if (columns.size() != COLUMNS_SIZE) {
            LOGGER.error(ErrorMessages.INVALID_COLUMNS_SIZE);
            return new FailedDeal(dealId, rawDeal, fileName, ErrorMessages.INVALID_COLUMNS_SIZE);
        }

        if (!isValidTimeStamp(columns.get(DEAL_TIMESTAMP_INDEX))) {
            LOGGER.error(ErrorMessages.DEAL_TIMESTAMP_ERROR);
            return new FailedDeal(dealId, rawDeal, fileName, ErrorMessages.DEAL_TIMESTAMP_ERROR);
        }

        if (!NumberUtils.isParsable(columns.get(DEAL_AMOUNT_INDEX))) {
            LOGGER.error(ErrorMessages.DEAL_AMOUNT_ERROR);
            return new FailedDeal(dealId, rawDeal, fileName, ErrorMessages.DEAL_AMOUNT_ERROR);
        }


        if (isBlank(columns.get(FROM_CURRENCY_INDEX)) || columns.get(FROM_CURRENCY_INDEX).length() != 3) {
            LOGGER.error(ErrorMessages.FROM_CURRENCY_CODE_ERROR);
            return new FailedDeal(dealId, rawDeal, fileName, ErrorMessages.FROM_CURRENCY_CODE_ERROR);
        }
        if (isBlank(columns.get(TO_CURRENCY_INDEX)) || columns.get(TO_CURRENCY_INDEX).length() != 3) {
            LOGGER.error(ErrorMessages.FROM_CURRENCY_CODE_ERROR);
            return new FailedDeal(dealId, rawDeal, fileName, ErrorMessages.TO_CURRENCY_CODE_ERROR);
        }
        return null;
    }

    private static String getRawDealString(List<String> columns) {
        if (columns == null || columns.isEmpty()) {
            return "";
        }
        StringBuilder rawDeal = new StringBuilder();
        for (String column : columns) {
            rawDeal.append(column + ",");
        }
        rawDeal.deleteCharAt(rawDeal.lastIndexOf(","));
        return rawDeal.toString();
    }
}
