package com.propgresssoft.bloomberg.core.exceptions;

/**
 * Exception thrown when error happened while validating the file
 */
public class FileValidationException extends Exception {


    public FileValidationException(String message) {
        super(message);
    }


}
