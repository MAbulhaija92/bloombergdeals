package com.propgresssoft.bloomberg.core.exceptions;

/**
 * Exception thrown when error happened while importing the file
 */
public class FileImportException extends Exception {

    public FileImportException(String message) {
        super(message);
    }

}
