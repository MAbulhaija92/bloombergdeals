package com.propgresssoft.bloomberg;

import com.propgresssoft.bloomberg.db.DealsDAO;
import com.propgresssoft.bloomberg.resources.DealsImportResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.ScanningHibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class BloombergDealsApplication extends Application<BloombergDealsConfiguration> {

    private final HibernateBundle<BloombergDealsConfiguration> hibernate = new ScanningHibernateBundle<BloombergDealsConfiguration>("com.propgresssoft.bloomberg.beans.deals") {
        @Override
        public DataSourceFactory getDataSourceFactory(BloombergDealsConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    public static void main(final String[] args) throws Exception {
        new BloombergDealsApplication().run(args);
    }

    @Override
    public String getName() {
        return "bloomberg-deals";
    }

    @Override
    public void initialize(final Bootstrap<BloombergDealsConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(new AssetsBundle("/assets", "/",
                "index.html", "static"));
    }

    @Override
    public void run(final BloombergDealsConfiguration configuration,
                    final Environment environment) {


        final DealsDAO dao = new DealsDAO(hibernate.getSessionFactory());
        final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);


        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,HEAD,GET,POST");

        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        environment.jersey().setUrlPattern("/deals/*");
        environment.jersey().register(MultiPartFeature.class);
        environment.jersey().register(new DealsImportResource(dao));
    }

}
