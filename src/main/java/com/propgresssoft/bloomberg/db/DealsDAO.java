package com.propgresssoft.bloomberg.db;


import com.propgresssoft.bloomberg.beans.ErrorMessages;
import com.propgresssoft.bloomberg.beans.deals.*;
import com.propgresssoft.bloomberg.core.business.DealsImportBusiness;
import com.propgresssoft.bloomberg.core.exceptions.FileImportException;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;

/**
 * this class provides the main operation to insert/get from DB
 */
public class DealsDAO extends AbstractDAO<DealMarker> {



    public DealsDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * method inserts success deals to the proper DB table
     * @param deals, valid deals fetched from csv file
     */
    public void importSuccessDeals(List<SuccessDeal> deals) {

       EntityManager entityManager = this.currentSession().getSessionFactory().createEntityManager();
        entityManager.getTransaction().begin();

        for (SuccessDeal deal : deals) {
            entityManager.persist(deal);
        }
       new Thread(() -> {entityManager.getTransaction().commit();}).start();

    }

    /**
     * method inserts failed deals to the proper DB table
     * @param deals, invalid deals fetched from csv file
     */
    public void importFailedDeals(List<FailedDeal> deals) {

        EntityManager entityManager = this.currentSession().getSessionFactory().createEntityManager();
        entityManager.getTransaction().begin();

        for (FailedDeal deal : deals) {
            entityManager.persist(deal);
        }
        new Thread(() -> {entityManager.getTransaction().commit();}).start();

    }

  /*    StatelessSession statelessSession = this.currentSession().getSessionFactory().openStatelessSession();
        Transaction transaction = statelessSession.beginTransaction();
        for (int i = 0; i < deals.size(); i++) {
            statelessSession.insert(deals.get(i));
        }
        transaction.commit();
        statelessSession.close();
    }*/

    /**
     * method inserts or update currencies counters in the DB
     * @param currencies, Map contains each currency with the recent imported deals from file
     */
    public void importCurrenciesSummary(Map<String, Integer> currencies) {

        for (Map.Entry<String, Integer> entry : currencies.entrySet()) {
            CurrencySummary currencySummary = this.currentSession().find(CurrencySummary.class, entry.getKey());
            if (currencySummary == null) {
                currencySummary = new CurrencySummary(entry.getKey(), entry.getValue());
                this.currentSession().persist(currencySummary);
            } else {
                currencySummary.setSuccessDeals(currencySummary.getSuccessDeals() + entry.getValue());
            }
        }

    }

    /**
     * this method insert the imported file information, to the proper table in DB
     * @param file, {@link ImportedCsvFile}, the file info to be inserted
     */
    public void insertFile(ImportedCsvFile file) {
        StatelessSession statelessSession = this.currentSession().getSessionFactory().openStatelessSession();
        Transaction transaction = statelessSession.beginTransaction();
        statelessSession.insert(file);
        transaction.commit();
        statelessSession.close();
    }

    /**
     * this method check and return requested file info
     * @param fileName, filename to get info for
     * @return {@link ImportedCsvFile}, if found other wise returns null
     */
    public ImportedCsvFile getFileIfImported(String fileName) {
        return this.currentSession().get(ImportedCsvFile.class, fileName);
    }

    public FileDeals getImportedFileResults(String fileName, int page) throws FileImportException {
        FileDeals fileDeals = new FileDeals();
        ImportedCsvFile importedCsvFile = getFileIfImported(fileName);
        if (importedCsvFile == null) {
            throw new FileImportException(ErrorMessages.FILE_NOT_FOUND);
        }
        fileDeals.setImportedFile(importedCsvFile);

        Query query = this.currentSession().createQuery("select deal from SuccessDeal deal where deal.fromFile=:fileName");
        query.setParameter("fileName", fileName);
        query.setMaxResults(DealsImportBusiness.DEFAULT_PAGE_SIZE);
        query.setFirstResult(page * DealsImportBusiness.DEFAULT_PAGE_SIZE);
        List<SuccessDeal> deals = query.list();
        fileDeals.setValidDeals(deals);
        return fileDeals;
    }
}
