-- -----------------------------------------------------
-- Schema deals
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `deals` ;

-- -----------------------------------------------------
-- Schema deals
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `deals` DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;
USE `deals` ;

CREATE TABLE `imported_files` (
  `file_name` varchar(255) NOT NULL,
  `success` int(10) NOT NULL,
  `failed` int(10) NOT NULL,
  PRIMARY KEY (`file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `success_imported_deals` (
  `deal_id` varchar (100) NOT NULL,
  `from_currency` varchar(3) NOT NULL,
  `to_currency` varchar(3) NOT NULL,
  `deal_date` datetime NOT NULL,
  `amount` double NOT NULL,
  `from_file` varchar(255) NOT NULL,
  PRIMARY KEY (`deal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

CREATE TABLE `failed_imported_deals` (
  `deal_id` varchar (100) NOT NULL,
  `deal_raw` varchar(500) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `from_file` varchar(255) NOT NULL,
  PRIMARY KEY (`deal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


CREATE TABLE `currency_statistics` (
  `currency_iso_code` varchar (3) NOT NULL,
  `success_deals_count` int(10) NOT NULL,
  PRIMARY KEY (`currency_iso_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

SET GLOBAL storage_engine=MYISAM;