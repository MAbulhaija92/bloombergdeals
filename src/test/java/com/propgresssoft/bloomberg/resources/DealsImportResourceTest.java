package com.propgresssoft.bloomberg.resources;


import com.propgresssoft.bloomberg.BaseTest;
import com.propgresssoft.bloomberg.beans.ErrorMessages;
import com.propgresssoft.bloomberg.beans.FailResponse;
import com.propgresssoft.bloomberg.beans.ImportResult;
import com.propgresssoft.bloomberg.beans.deals.FileDeals;
import com.propgresssoft.bloomberg.beans.deals.ImportedCsvFile;
import io.dropwizard.hibernate.UnitOfWork;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;
import static java.time.Duration.ofSeconds;
import static org.junit.jupiter.api.Assertions.*;

public class DealsImportResourceTest extends BaseTest {


    @Test
    @UnitOfWork
    public void testImportInvalidFile(){

        final FormDataMultiPart multiPart = new FormDataMultiPart();

        FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file", new File(resourceFilePath("testFiles/dummy.txt")));

        multiPart.bodyPart(fileDataBodyPart);

        Response firstImportResponse = dropwizardExtension.client().register(MultiPartFeature.class).target("http://localhost:" +
                dropwizardExtension.getLocalPort() + "/deals/import")
                .request(MediaType.APPLICATION_JSON)
                .post(javax.ws.rs.client.Entity.entity(multiPart, multiPart.getMediaType()), Response.class);


        assertEquals(400, firstImportResponse.getStatus());
        FailResponse failResponse = firstImportResponse.readEntity(FailResponse.class);
        assertNotNull(failResponse);
        assertEquals(ErrorMessages.NOT_SUPPORTED_FILE_TYPE, failResponse.getMessage());
        assertEquals(400, failResponse.getStatus());
    }

    @Test
    @UnitOfWork
    public void testResourceEndpoints(){
        assertTimeout(ofSeconds(20), () -> {

            final FormDataMultiPart multiPart = new FormDataMultiPart();

            FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file", new File(resourceFilePath("testFiles/10rows.csv")));

            multiPart.bodyPart(fileDataBodyPart);

            Response firstImportResponse = dropwizardExtension.client().register(MultiPartFeature.class).target("http://localhost:" +
                    dropwizardExtension.getLocalPort() + "/deals/import")
                    .request(MediaType.APPLICATION_JSON)
                    .post(javax.ws.rs.client.Entity.entity(multiPart, multiPart.getMediaType()), Response.class);

            assertEquals(200, firstImportResponse.getStatus());
            ImportResult importResult = firstImportResponse.readEntity(ImportResult.class);
            assertNotNull(importResult);

            Response secondImportResponse = dropwizardExtension.client().register(MultiPartFeature.class).target("http://localhost:" +
                    dropwizardExtension.getLocalPort() + "/deals/import")
                    .request(MediaType.APPLICATION_JSON)
                    .post(javax.ws.rs.client.Entity.entity(multiPart, multiPart.getMediaType()), Response.class);
            assertEquals(400, secondImportResponse.getStatus());

            FileDeals fileResult = dropwizardExtension.client().register(MultiPartFeature.class).target("http://localhost:" +
                    dropwizardExtension.getLocalPort() + "/deals/info")
                    .queryParam("file_name","10rows")
                    .queryParam("page",0)
                    .request(MediaType.APPLICATION_JSON)
                    .get(FileDeals.class);

            assertNotNull(fileResult);
            ImportedCsvFile importedFile = fileResult.getImportedFile();
            assertEquals("10rows", importedFile.getFileName() );
            assertEquals(0, importedFile.getFailed() );
            assertEquals(10, importedFile.getSuccess() );

        });
    }
}
