package com.propgresssoft.bloomberg.core;


import com.propgresssoft.bloomberg.BaseTest;
import com.propgresssoft.bloomberg.beans.ErrorMessages;
import com.propgresssoft.bloomberg.beans.deals.CurrencySummary;
import com.propgresssoft.bloomberg.beans.deals.FailedDeal;
import com.propgresssoft.bloomberg.beans.deals.FileDeals;
import com.propgresssoft.bloomberg.beans.deals.SuccessDeal;
import com.propgresssoft.bloomberg.core.business.DealsImportBusiness;
import com.propgresssoft.bloomberg.core.exceptions.FileImportException;
import com.propgresssoft.bloomberg.core.exceptions.FileValidationException;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;

public class DealsImportBusinessTest extends BaseTest {

    @Test
    public void testFetchDealsFromCSV() throws IOException, FileImportException {
        File file = new File(resourceFilePath("testFiles/15rows.csv"));
        FileDeals fileDeals = DealsImportBusiness.fetchDealsFromCSV(new FileInputStream(file), "15rows.csv");

        Assertions.assertAll(() -> {
                    Assertions.assertNotNull(fileDeals);
                    Assertions.assertEquals(2, fileDeals.getCurrencies().size());
                    Assertions.assertEquals(5, fileDeals.getInValidDeals().size());
                    Assertions.assertEquals(10, fileDeals.getValidDeals().size());
                    Assertions.assertEquals("15rows", fileDeals.getImportedFile().getFileName());

                    SuccessDeal successDeal = fileDeals.getValidDeals().get(0);
                    FailedDeal failedDeal = fileDeals.getInValidDeals().get(0);
                    CurrencySummary currencySummary = new CurrencySummary("JOD", 5);

                    Assertions.assertEquals("AMJQ123aSD", successDeal.getDealId());
                    Assertions.assertEquals("JOD", successDeal.getFormCurrencyISO());
                    Assertions.assertEquals("USD", successDeal.getToCurrencyISO());
                    Assertions.assertEquals(Timestamp.valueOf("2019-04-10 10:10:10"), successDeal.getDealTimeStamp());
                    Assertions.assertEquals(1000, successDeal.getDealAmount());

                    Assertions.assertEquals("SCTHHX2pbG,USD,JOD,2019-04-10 10:10:10,acs", failedDeal.getDealRaw());
                    Assertions.assertEquals("SCTHHX2pbG", failedDeal.getDealId());
                    Assertions.assertEquals(ErrorMessages.DEAL_AMOUNT_ERROR, failedDeal.getReason());

                    Assertions.assertEquals("JOD", currencySummary.getCurrencyCode());
                    Assertions.assertEquals(fileDeals.getCurrencies().get("JOD"), currencySummary.getSuccessDeals());
                    Assertions.assertTrue(currencySummary.equals(new CurrencySummary("JOD",5)));

                }
        );
    }


    @Test
    public void testValidateCSVFile() throws IOException, ParseException {
        FormDataContentDisposition validFormData = new FormDataContentDisposition("form-data; filename=anyCsv.csv; size=442; name=file");
        try {
            DealsImportBusiness.validateCSVFile(validFormData);
        } catch (FileValidationException e) {
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void testInValidCSVFile() throws ParseException {
        FormDataContentDisposition inValidFormDataType = new FormDataContentDisposition("form-data; filename=anyText.txt; size=442; name=file");
        FormDataContentDisposition inValidFormDataSize = new FormDataContentDisposition("form-data; filename=anyText.csv; size=0; name=file");

        Assertions.assertThrows(FileValidationException.class, () -> DealsImportBusiness.validateCSVFile(inValidFormDataType));
        Assertions.assertThrows(FileValidationException.class, () -> DealsImportBusiness.validateCSVFile(inValidFormDataSize));
    }

    @Test
    public void testGetFileNameWithoutType(){
        String fileNameWithType1 = "abc.csv";
        String fileNameWithType2 = "abc.txt";
        Assertions.assertEquals("abc", DealsImportBusiness.getFileNameWithoutType(fileNameWithType1));
        Assertions.assertEquals(fileNameWithType2, DealsImportBusiness.getFileNameWithoutType(fileNameWithType2));

    }

}
