package com.propgresssoft.bloomberg;


import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class SetUpTestingDB implements BeforeAllCallback {

    private final static Logger logger = LoggerFactory.getLogger(SetUpTestingDB.class);
    private static final String dbHome = "/tmp/tempDealsDB";
    private final static AtomicInteger beforeAllCallCount = new AtomicInteger(0);
    private DB db;

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {


        if (beforeAllCallCount.getAndIncrement() != 0) {
            return;
        }
        cleanIfExist(dbHome);

        DBConfigurationBuilder builder = DBConfigurationBuilder.newBuilder();

        builder.setBaseDir(dbHome + File.separator + "base");
        builder.setDataDir(dbHome + File.separator + "data");

        builder.setDatabaseVersion("mariadb-10.2.11");
        builder.setPort(9000);

        builder.addArg("--character-set-server=utf8mb4");
        builder.addArg("--collation-server=utf8mb4_unicode_ci");

        db = DB.newEmbeddedDB(builder.build());
        db.start();

        db.source("deals-schema.sql");
    }


    private void cleanIfExist(String path) throws IOException {
        File file = new File(path);

        if (!file.exists() || !file.isDirectory()) return;

        FileUtils.deleteDirectory(file);

    }

}

