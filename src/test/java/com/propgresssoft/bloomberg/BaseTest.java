package com.propgresssoft.bloomberg;

import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;

@ExtendWith(SetUpTestingDB.class)
public class BaseTest {

    public static final DropwizardAppExtension<BloombergDealsConfiguration> dropwizardExtension =
            new DropwizardAppExtension<>(
                    BloombergDealsApplication.class, resourceFilePath("testConfig.yml"),
                    ConfigOverride.config("server.applicationConnectors[0].port", "0"),
                    ConfigOverride.config("server.adminConnectors[0].port", "0"));

    @BeforeAll
    public static void init() {
        dropwizardExtension.before();
    }

}
